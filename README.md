# SEm-225 I5 CI/TDD lab

[![pipeline status](https://gitlab.com/MorganePorchet/lab-ci/badges/master/pipeline.svg)](https://gitlab.com/MorganePorchet/lab-ci/-/commits/master)

## Abstract
This repository is used for "CI/TDD" lab of the S.Em. 225 module at HES-SO Valais-Wallis in Sion.

## Getting Started
Fork this repository from the appropriate git source server to have your own working copy. Contribute with your own additions in your own repository.

## Prerequisites
A local installation of git is required.
In order to compile this project and run the tests locally, GCC and Ruby must be installed, or a Docker image can be used.

## Running the tests
There are a number of simple automated (using GitLab CI) tests in this repository.

## Built With

* Git - A distributed version-control system for tracking changes in source code

* GitLab - a web-based DevOps life cycle tool that provides a Git repository manager

* GitLab CI - The Continuous Integration tool suite available in GitLab

* C - A general-purpose, procedural computer programming language

* Unity - A unit test framework for the C programming language (Especially Embedded Software) [http://www.throwtheswitch.org/unity](http://www.throwtheswitch.org/unity)

* Ruby - An interpreted, high-level, general-purpose programming language

## Contributing
To contribute, fork this repository first and then add content in your own copy of the repository.

## Authors
* Steve Devenes
* Jerome Corre
* Christopher Métrailler

## License
Copyright (C) 2023 HES-SO Valais-Wallis - All Rights Reserved
