#include "average.h"
#include "unity.h"

#include <stdlib.h>
#include <string.h>

void setUp(void)
{

}

void tearDown(void)
{

}

void test_average_function(void)
{
  /* Check result */
  TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_average(0, 0), "Error in integer_average");
  TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_average(1, 0), "Error in integer_average");
  TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_average(1, 1), "Error in integer_average");
  TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_average(1, 2), "Error in integer_average");
  TEST_ASSERT_EQUAL_INT_MESSAGE(1, integer_average(2, 1), "Error in integer_average");
  TEST_ASSERT_EQUAL_INT_MESSAGE(0, integer_average(-1, 0), "Error in integer_average");
  TEST_ASSERT_EQUAL_INT_MESSAGE(-1, integer_average(-1, -1), "Error in integer_average");
}
