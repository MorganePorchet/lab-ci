#ifndef __AVERAGE_H
#define __AVERAGE_H
/** @return The average of two integers @ref a and @ref b.*/
int integer_average(int a, int b);
#endif //__AVERAGE_H
